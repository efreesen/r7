class MessagesPresenter
  def initialize(flash)
    @flash = flash
  end

  def flashes
    @flash.to_a.map do |type, messages|
      bla(type, messages)
    end
  end

  private
  def bla(type, messages)
    if messages.is_a?(String)
      [type, Array(messages)]
    else
      messages_list(type, messages)
    end
  end

  def messages_list(type, messages)
    messages = messages.map do |message|
      errors(type, message.first).map do |error|
        "#{message.first.capitalize} #{error}"
      end
    end

    [type, messages.flatten]
  end

  def errors(type, field)
    @flash[type.to_sym][field.to_sym]
  end
end
