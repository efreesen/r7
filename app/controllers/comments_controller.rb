class CommentsController < ApplicationController
  def create
    @comment = Post::Comment.new(comment_params)

    if @comment.save
      flash[:notice] = "Thread succesfully created"
    else
      flash[:error] = @comment.errors.messages
    end

    redirect_to @comment.forum_thread
  end

  private
  def comment_params
    params.require(:post_comment).permit(:content, :parent_id)
  end
end