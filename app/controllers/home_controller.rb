class HomeController < ApplicationController
  def index
    @thread = Post::ForumThread.new
    @threads = Post::ForumThread.paginate(page: params[:i], per_page: 10)
  end
end