class ForumThreadsController < ApplicationController
  def create
    @thread = Post::ForumThread.new(thread_params)

    if @thread.save
      flash[:notice] = "Thread succesfully created"
    else
      flash[:error] = @thread.errors.messages
    end

    redirect_to root_path
  end

  def show
    @thread = Post::ForumThread.find(params[:id])
    @posts = Post.where("id = ? or forum_thread_id = ?", @thread.id, @thread.id).order('position, parent_id').paginate(page: params[:i], per_page: 10)
  end

  private
  def thread_params
    params.require(:post_forum_thread).permit(:content)
  end
end