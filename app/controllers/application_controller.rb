class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :messages_presenter

  def messages_presenter
  	@messages_presenter ||= MessagesPresenter.new(flash)
  end
end
