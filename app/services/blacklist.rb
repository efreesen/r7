class Blacklist
  WORDS = {"carro" => "xxxxx", "arma" => "xxxx"}
  REPLACE_CHARS = 'aaaaaaaaaaaaeeeeeeeeeiiiiiiiiiioooooooooooouuuuuuuusstn'
  TARGET_CHARS  = '@4áâãàäÁÂÃÀÄ3éêèëÉÊÈË1!íîìïÍÎÌÏ0*óôõòöÓÔÕÒÖúûùüÚÛÙÜ$57ñ'

  def self.execute!(content)
    return '' if content.blank?
    content.tr!(TARGET_CHARS, REPLACE_CHARS)

    content = content.downcase.split(' ')

    content.map{ |word| WORDS.keys.include?(word) ? WORDS[word] : word }.join(' ')
  end
end