class Post < ActiveRecord::Base
  has_many :comments, foreign_key: :parent_id
  validates_presence_of :content
  before_create :set_position, :check_blacklist

  protected
  def next_position
    parent.comments.maximum('position').to_i + 1
  end

  private
  def set_position
    self.position = next_position
  end

  def check_blacklist
    self.content = Blacklist.execute!(content)
  end
end
