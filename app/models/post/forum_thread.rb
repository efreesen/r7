class Post::ForumThread < Post
  def positions
    position.to_s
  end

  def positions_size
    1
  end

  def forum_thread
  	nil
  end

  private
  def next_position
    ForumThread.maximum('position').to_i + 1
  end
end
