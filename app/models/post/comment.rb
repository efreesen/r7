class Post::Comment < Post
  belongs_to :forum_thread, class_name: 'Post'
  belongs_to :parent, class_name: 'Post'
  has_many :comments, foreign_key: :parent_id
  validates_presence_of :parent_id
  before_create :set_forum_thread_id

  def positions
    parent.nil? ? parent.id.to_s : "#{parent.positions}.#{position.to_s}"
  end

  def positions_size
    positions.split('.').size
  end

  private
  def set_forum_thread_id
    self.forum_thread_id = parent.is_a?(Post::ForumThread) ? parent.id : parent.forum_thread_id
  end
end
