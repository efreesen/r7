$('document').ready(function () {
  $('.button.submit').click(function (e) {
    e.preventDefault();
    target = $(e.target);
    target.parents('form').submit();
  })

  $('.button.reply').click(function (e) {
    e.preventDefault();
    $('.children').hide();
    $('#form_' + $(this).data('id')).show();
  })
});