class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.string :type
      t.integer :position
      t.integer :forum_thread_id, index: true
      t.integer :parent_id, index: true

      t.timestamps
    end
  end
end
