R7::Application.routes.draw do
  resources :post_comments, only: [:create], controller: 'comments'
  resources :post_forum_threads, only: [:create], controller: 'forum_threads'

  match "/:id" => "forum_threads#show", via: :get, as: 'post_forum_thread'

  root 'home#index'
end
